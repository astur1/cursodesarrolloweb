<?php
class Numeros {
    private $valores=[];
    private $suma;
    private $media;
    private $producto;
    public function __construct($datos) {
      $this->valores=$datos;  
    }  
    
    public function getValores() {
        return $this->valores;
    }
    
    public function getValores1(){
        return "<div>". join(",", $this->valores)."</div>";
    }

    public function setValores($valores) {
        $this->valores = $valores;
        return $this;
    }

    public function sumar(){
        return array_sum($this->valores); 
    }
    
    public function media(){
        return $this->sumar()/ count($this->valores);
    }
    
    public function producto(){
        return array_product($this->valores);
    }
    public function getSuma() {
        $this->suma=$this->sumar();
        return "<div>".$this->suma."</div>";
    }

    public function getMedia() {
        $this->media= $this->media();
        return "<div>". $this->media . "</div>";
    }

    public function getProducto() {
        $this->producto= $this->producto();
        return "<div>".$this->producto."</div>";
    }

    public function setSuma($suma) {
        $this->suma = $suma;
        return $this;
    }

    public function setMedia($media) {
        $this->media = $media;
        return $this;
    }

    public function setProducto($producto) {
        $this->producto = $producto;
        return $this;
    }

    public static function sumaEstatica($v){
       return array_sum($v); 
    }

}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos".
 *
 * @property int $ID
 * @property string|null $nombre
 * @property string|null $foto
 * @property string|null $descripcion
 * @property float|null $precio
 * @property int|null $oferta
 */
class Productos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['precio'], 'number'],
            [['oferta'], 'integer'],
            [['nombre'], 'string', 'max' => 15],
            [['foto'], 'string', 'max' => 20],
            [['descripcion'], 'string', 'max' => 40],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'nombre' => 'Nombre',
            'foto' => 'Foto',
            'descripcion' => 'Descripcion',
            'precio' => 'Precio',
            'oferta' => 'Oferta',
        ];
    }
}

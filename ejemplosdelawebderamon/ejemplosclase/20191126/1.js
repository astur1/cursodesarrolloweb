/**
 * opcion 1
 * 
 */
function funcion1Opcion1(){
	for (let i = 0; i < 5; i++) {
		document.write("<div>" + i + "</div>");
	}
}

/**
 * opcion 2
 * 
 */
function funcion1Opcion2(){
	let salida="";
	for (let i = 0; i < 5; i++) {
		salida+="<div>" + i + "</div>";
	}
	return salida;
}

/**
 * opcion 3
 * 
 */

function funcion1Opcion3(){
	let caja=document.querySelector(".dos");
	let salida="";
	for (let i = 0; i < 5; i++) {
		salida+="<div>" + i + "</div>";
	}
	caja.innerHTML=salida;
}


/**
 * Para recorrer array asociativos
 * es decir para JSON (objetos)
 * y arrays enumerados
 */

function funcion2(v){
	let salida="";
	for (i in v) {
		salida+="<div id=\""+ i+"\">"+ v[i] +"</div>";
	}
	return salida;
}

/**
 * Para recorrer arrays enumerados
 * no funciona con JSON
 */
v=[2,3,4,5];
function funcion2Opcion1(v){
	let salida="";
	v.forEach(function(valor,indice){
		salida+="<div id=\""+ indice+"\">"+ valor +"</div>";
	});
	return salida;
}


/**
 * Para recorrer arrays enumerados
 * no funciona con JSON
 */

function funcion2Opcion2(v){
	let salida="";
	for(let c=0;c<v.length;c++){
		salida+="<div id=\""+ c +"\">"+ v[c] +"</div>";
	}
	
	return salida;
}


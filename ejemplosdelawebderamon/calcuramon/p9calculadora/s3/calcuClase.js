
let global={
	limpiar:false,
	operacion:null,
	resultado:0
};

window.addEventListener("load",(e)=>{
	global.operaciones=document.querySelectorAll(".operacion");
	global.numeros=document.querySelectorAll(".numero");
	global.display=document.querySelector(".display");

	for(var c=0;c<global.operaciones.length;c++){
		global.operaciones[c].addEventListener("click",(e) => {
			if(!global.limpiar || global.operacion == e.target.value){
				  switch(global.operacion){
				  	case '+':
				  		global.resultado+=parseInt(global.display.value);
				  		global.display.value=global.resultado;
				  		break;
				  	case '-':
				  		global.resultado-=parseInt(global.display.value);
				  		global.display.value=global.resultado;
				  		break;
				  	default:
				  		global.resultado=parseInt(global.display.value);
				  }
			}
		  global.operacion=e.target.value;
		  global.limpiar=true;
		});
	}

	for(var c=0;c<global.numeros.length;c++){
		global.numeros[c].addEventListener("click",(e) => {
			if(global.limpiar){
				global.display.value="";
				global.limpiar=false;
			}
			global.display.value+=e.target.value;	  
		});
	}
});
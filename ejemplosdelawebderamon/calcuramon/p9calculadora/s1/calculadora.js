// JavaScript Document

/**
*** Creamos un objeto mediante JSON para almacenar las variables globales
**/
var variables={
};


/**
*** Inicializacion
**/

window.addEventListener("load",inicio);

function inicio(){
	variables.calculadora=new Calculadora(formulario.display);
	variables.asociarNumeros=new Asociar(
									document.getElementsByClassName("numero"),
									"click",
									"variables.calculadora.numeros"
									);
	variables.asociarOperaciones=new Asociar(
									document.getElementsByClassName("operacion"),
									"click",
									"variables.calculadora.operaciones"
									);
}

/**
*** Fin de la inicializacion
**/


/**
*** Creamos una clase que denominamos Asociar
*** con esta clase asociamos a un formulario 
*** a todos sus botones un metodo cuando realizamos algun evento
**/

var Asociar=function(objeto,evento,funcion){
	this.objeto=objeto;
	this.evento=evento;
	this.funcion=funcion + "(this)";
	this.colocar=function(){
		for(var i=0;i<this.objeto.length;i++){
			var temporal=new Function(this.funcion);
			this.objeto[i].addEventListener(this.evento,temporal);
	}
	};
	/* hacemos que automaticamente se ejecute colocar (hace de constuctora) */
	this.colocar();
}



/**
*** Creamos la clase calculadora
*** Debes pasarle como argumento el display donde se mostraran los datos 
**/ 


var Calculadora=function(display){
	/*miembros publicos */
	this.operaciones=function(objeto){
		if(!limpia){ //comprobamos que no vengamos de apretar una operacion
			if(acumulador==0){ //es la primera vez que pulsamos una operacion
				acumulador=parseInt(display.value);
			}else{
				switch(acumuladorOperacion){
					case '+':
						acumulador=acumulador + parseInt(display.value);
						break;
					case '-':
						acumulador=acumulador - parseInt(display.value);
						break;
					case '*':
						acumulador=acumulador * parseInt(display.value);
						break;
					case '/':
						acumulador=acumulador / parseInt(display.value);
						break;
					default:
						acumulador=parseInt(display.value);
					
				};
				display.value=acumulador;
				
			}
		}
		limpia=true;
		acumuladorOperacion=objeto.value;
	};
	this.numeros=function(objeto){
		if(limpia){
			display.value=objeto.value;
			limpia=false;
		}else{
			display.value=display.value+objeto.value;
		}
	};

	/* Miembros privados */
	var acumulador=0;
	var acumuladorOperacion='';
	var limpia=false;
};


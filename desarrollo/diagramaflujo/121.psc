Algoritmo ordenacion
	// ordenar de forma descendente
	n1 <- 0
	n2 <- 0
	n3 <- 0
	mayor <- 0
	mediano <- 0
	peque <- 0
	Escribir 'introduce un numero'
	Leer n1
	Escribir 'introduce otro numero'
	Leer n2
	Escribir 'introduce otro numero'
	Leer n3
	Si n1>=n2 Entonces
		Si n1>n3 Entonces
			mayor <- n1
			Si n2>n3 Entonces
				mediano <- n2
				peque <- n3
			SiNo
				mediano <- n3
				peque <- n2
			FinSi
		SiNo
			mayor <- n3
			mediano <- n1
			peque <- n2
		FinSi
	SiNo
		Si n2>n3 Entonces
			mayor <- n2
			Si n1>n3 Entonces
				mediano <- n1
				peque <- n3
			SiNo
				mediano <- n3
				peque <- n1
			FinSi
		SiNo
			mayor <- n3
			mediano <- n2
			peque <- n1
		FinSi
	FinSi
	Escribir mayor,mediano,peque
FinAlgoritmo


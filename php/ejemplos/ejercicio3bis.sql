﻿CREATE DATABASE IF NOT EXISTS jota;

USE jota;
CREATE TABLE productos(
  ID int AUTO_INCREMENT,
  nombre varchar(15),
  foto varchar(20),
  descripcion varchar(40),
  precio float,
  oferta boolean,
  PRIMARY KEY (ID)
  );

INSERT INTO productos (nombre, foto, descripcion, precio, oferta)
  VALUES ('casa','una','casa1',20000,FALSE),
          ('coche','dos','coche2',30000,TRUE),
          ('bici','tres','bici3',4000,FALSE),
          ('cuadro','cuatro','cuadro4',50000,TRUE),
          ('reloj','cinco','reloj5',60000,FALSE)
;



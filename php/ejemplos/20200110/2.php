<?php
//estos no son objetos. son datos primitivos sin propiedades
$contador=0;
var_dump($contador);/*significa que te de informacion*/

$texto=null;
$texto=$texto.="a";
var_dump($texto);

$cadena="";
var_dump($cadena);

$cadena=10;
var_dump($cadena);

$cadena="ejemplo";/*la variable cadena vale el string ejemplo*/

$longitud=strlen($cadena);/*strlen es una funcion*/
var_dump($longitud);

$a=[];/*esto es un array vacio con la variable a */

$a=[1,2,3];/*dos formas diferentes de crear un array*/
echo '<pre>';/*pre es por si no tienes un depurador*/
var_dump($a);
echo '</pre>';

$b=array(1,2,3);
var_dump($b);
function depurar($v) {/*vamos a crear una funcion*/
 echo "<pre>";
 var_dump($v);
 echo '</pre>';
}
$vocales=["a","e","i","o","u"];/*vamos a crear un array asociativo*/
$repeticiones=[
    'a'=>23,/*el signo => equivale mas o menos a equivale*/
    'e'=>1,
    'i'=>0,
    'o'=>40,
    'u'=>10
];
depurar($vocales);
depurar($repeticiones);
$repeticiones['o']++;
 depurar($repeticiones);       

/* 
 
 */

